import { ACTIONS } from '../../constants';

const initialState = {
  code: null,
  message: '',
};

export default function reducer(state = initialState, action) {
  const { LOGIN_FAILED } = ACTIONS;
  const { type, code, message } = action;

  switch (type) {
    case LOGIN_FAILED:
      return {
        ...state,
        code,
        message
      };
    default:
      return state;
  }
}
