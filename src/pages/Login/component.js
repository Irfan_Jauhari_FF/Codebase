import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import PropTypes from 'prop-types';
import LoginForm from '../../components/forms/Login';
import { ICONS, IMAGES } from '../../configs';
import errMessage from '../../utils/errorAuthentication';
import styles from './styles.css';

export default class Component extends React.Component {
  constructor() {
    super();
    this.state = {
      openSnackbar: true,
    };

    this._handleSubmit = this._handleSubmit.bind(this);
  }

  _handleSubmit(values = {}) {
    const { actions } = this.props;
    const payload = {
      nik: values.nik,
      password: values.password,
    };
    
    actions.fetchLogin(payload);
    this.setState({ openSnackbar: true });
  }

  _handleClickCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ openSnackbar: false });
  }

  _renderSnackbar() {
    const { code } = this.props;

    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        autoHideDuration={6000}
        className={styles['snackbar']}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        onClose={this._handleClickCloseSnackbar}
        open={this.state.openSnackbar}
      >
        <SnackbarContent
          action={[
            <IconButton
              aria-label="Close"
              color="inherit"
              key="close"
              onClick={this._handleClickCloseSnackbar}
            >
              <CloseIcon className={styles['snackbar-icon']} />
            </IconButton>,
          ]}
          aria-describedby="client-snackbar"
          className={styles['snackbar-content']}
          message={
            <span className={styles['snackbar-message']} id="client-snackbar">
              <img className={styles['snackbar-icon']} src={ICONS.SNACKBAR_WARNING} />
              <h5>{code ? errMessage(code) : ''}</h5>
            </span>
          }
        />
      </Snackbar>
    );
  }

  render() {
    const { code, message } = this.props;

    return (
      <main className={styles.container}>
        {message ? this._renderSnackbar() : null}
        <section 
          className={styles.background} 
          style={{ backgroundImage: `url(${IMAGES.BG_LOGIN})` }}
        >
          <img className={styles['logo']} src={IMAGES.LOGO} />
          <h2 className={styles['product-name']}>Performance Dashboard</h2>
          <img className={styles['content-image']}src={IMAGES.LOGIN_BACKGROUND} />
        </section>
        <section className={styles.card}>
          <h3 className={styles.tagline}>Hello, Welcome back!</h3>
          <LoginForm code={code} id="loginForm" onSubmit={this._handleSubmit} />
        </section>
      </main>
    );
  }
}

Component.defaultProps = {
  actions: PropTypes.object,
  code: PropTypes.number,
  message: PropTypes.string,
};

Component.propTypes = {
  actions: PropTypes.object,
  code: PropTypes.number,
  message: PropTypes.string,
};
