import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { currency, numberWithPoint } from '../../utils/format';
import AchievementCard from '../../components/fragments/AchievementCard';
import BreadCrumb from '../../components/elements/Breadcrumb';
import PageBase from '../../components/layouts/PageBase';
import Tabs from '../../components/elements/Tabs';
import TalentDetailsCard from '../../components/fragments/TalentDetailsCard';
import styles from './styles.css';

export default class Component extends React.Component {
  componentDidMount() {
    const { match } = this.props;

    this.props.actions.fetchData(match.params.id, 'details', {});
    this.props.actions.fetchData(match.params.id, 'talent', []);
    this.props.actions.fetchData(match.params.id, 'achievement', []);
  }

  componentDidUpdate(nextProps) {
    const { actions, match } = this.props;

    if (nextProps.location.search !== location.search) {
      actions.fetchData(match.params.id, 'achievement');
    }
  }

  _renderDescription() {
    const { data, isLoading } = this.props;
    let breadcrumbItems = [
      { text: 'Project', url: '/project' },
      { text: isLoading.details ? '' : data.details.projectName },
    ];

    if (isLoading.details) return null;

    let descProduct = <strong className={styles['desc-product']}>{data.details.productName}</strong>;
    let descSquad = <strong className={styles['desc-project']}> {data.details.squadName}</strong>;
    let descDate = <strong className={styles['desc-date']}>{moment(data.details.startDate).locale('id').format('L')}</strong>;
    let descOmtm = <strong className={styles['desc-omtm']}>{data.details.metric.metricName}&nbsp;({data.details.metric.metricProgress}% of&nbsp;{numberWithPoint(data.details.metric.metricTarget)}&nbsp;{data.details.metric.metricUnit})</strong>;
    let descBudget = <strong className={styles['desc-budget']}> {data.details.budget.budgetRealization}%&nbsp;of {currency(data.details.budget.budgetTotal)}</strong> ;
    
    return (
      <>
        <BreadCrumb data={breadcrumbItems} />
        <h3 className={styles.title}>{data.details.projectName}</h3>
        <header className={styles['header-description']}>
          <div className={styles['header-product']}>
            <h6><span className={styles['font-title']}>Product Name</span>{descProduct}</h6>
            <h6><span className={styles['font-title']}>Squad Name</span> {descSquad}</h6>
            <h6><span className={styles['font-title']}>Start Date</span>{descDate}</h6>
          </div>
          <div>
            <h6><span className={styles['font-title']}>OMTM Achievement</span>{descOmtm}</h6>
            <h6><span className={styles['font-title']}>Budget Realization</span>{descBudget}</h6>
          </div>
        </header>
      </>
    );
  }

  _renderAchievement() {
    const { data, isLoading } = this.props;

    if (isLoading.details) return null;

    return (
      <>
        {!isLoading.details ? (
          <>
            <header className={styles['header-description']}>
              <div className={styles['content-description']}>
                <h6 className={styles['title-description']}>Total Objective</h6>
                <h5 className={styles['title-memberCount']}><strong>{data.details.objectiveTotal}</strong></h5>
              </div>
            </header>
          </>
        ) : ''}
        <AchievementCard
          data={data.achievement}
          isLoading={isLoading.achievement}
        />
      </>
    );
  }

  _renderTalent() {
    const { data, isLoading } = this.props;

    return (
      <>
        {!isLoading.details ? (
          <header className={styles['header-description']}>
            <div className={styles['content-description']}>
              <h6 className={styles['title-description']}>Total Member</h6>
              <h5 className={styles['title-memberCount']}><strong>{data.details.memberCount}</strong></h5>
            </div>
          </header>
        ) : ''}
        <TalentDetailsCard data={data.talent} isLoading={isLoading.talent} />
      </>
    );
  }

  render() {
    return (
      <PageBase>
        <main>
          {this._renderDescription()}
          <Tabs
            content={[
              this._renderTalent(),
              this._renderAchievement()
            ]}
            title={['Member', 'OKR']}
          />
        </main>
      </PageBase>
    );
  }
}

Component.defaultProps = {
  actions: {},
  data: {},
  isLoading: {},
  match: {},
};

Component.propTypes = {
  actions: PropTypes.object,
  data: PropTypes.object,
  isLoading: PropTypes.object,
  location: PropTypes.shape({
    search: PropTypes.string.isRequired,
  }).isRequired,
  match: PropTypes.object,
};
