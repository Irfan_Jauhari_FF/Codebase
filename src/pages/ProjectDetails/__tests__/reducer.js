import reducer from '../reducer';
import { ACTIONS } from '../../../constants';

const assert = {
  data: {
    details: null,
    talent: null,
    achievement: null,
  },
  isLoading: {
    details: true,
    talent: true,
    achievement: true,
  },
};

describe('Project Detail reducer', () => {
  it('return desired state when given LOADING action in Tribe filter', () => {
    const action = { type: ACTIONS.LOADING, name: 'details' };
    expect(reducer(undefined, action)).toHaveProperty('isLoading', assert.isLoading);
  });

  it('returns desired state when given LIST_PROJECT_DETAILS_FETCHED action', () => {
    const action = {
      type: ACTIONS.LIST_PROJECT_DETAILS_FETCHED,
      name: 'details',
      data: {}
    };
    const assert = {
      data: {
        details: {},
        talent: null,
        achievement: null,
      },
      isLoading: {
        details: false,
        talent: true,
        achievement: true,
      },
    };
    expect(reducer(undefined, action)).toMatchObject(assert);
  });

  it('returns initial state when given other actions', () => {
    const action = { type: 'x' };
    expect(reducer(undefined, action)).toMatchObject(assert);
  });
});
