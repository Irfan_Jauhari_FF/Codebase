import React from 'react';
import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import ProjectDetails from '../index';

jest.mock('../action');
jest.mock('../component');

describe('ProjectDetails', () => {
  it('has connected redux props', () => {
    const mockStore = configureMockStore([thunk]);
    const initialState = {
      projectDetails: {}
    };
    const assert = {
      actions: {},
      isLoading: {},
      data: {},
    };
    initialState.projectDetails = {
      isLoading: {},
      data: {},
    };
    const store = mockStore(initialState);
    const wrapper = shallow(<ProjectDetails store={store} />);
    
    expect(wrapper.props()).toMatchObject(assert);
  });
});
