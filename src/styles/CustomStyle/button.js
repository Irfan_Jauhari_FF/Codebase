export default {
  root: {
    height: '1.875rem',
    margin: 0,
    padding: 0,
    width: '6.25rem',
  },
};