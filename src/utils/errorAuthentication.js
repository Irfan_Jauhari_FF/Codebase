export default function errMessage(code) {
  let result = '';
  switch(code) {
    case 404 :
      result = 'Oops, the NIK or password you entered is incorrect.';
      break;
    case 401 :
      result = 'Oops, the NIK or password you entered is incorrect.';
      break;
    default:
      result = 'No value found.';
  } 

  return result;
}
