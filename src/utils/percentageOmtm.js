export const dataPercentageOmtm = [
  {
    id: undefined,
    name: 'All'
  },
  {
    id: '0',
    name: '0 - 10%'
  },
  {
    id: '1',
    name: '11 - 20%'
  },
  {
    id: '2',
    name: '21 - 30%'
  },
  {
    id: '3',
    name: '31 - 40%'
  },
  {
    id: '4',
    name: '41 - 50%'
  },
  {
    id: '5',
    name: '51 - 60%'
  },
  {
    id: '6',
    name: '61 - 70%'
  },
  {
    id: '7',
    name: '71 - 80%'
  },
  {
    id: '8',
    name: '81 - 90%'
  },
  {
    id: '9',
    name: '91 - 100%'
  }
];
