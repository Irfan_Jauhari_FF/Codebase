import icons from './icons';
import images from './images';
import routes from './routes';
import services from './services';
import text from './text';

export const ICONS = icons;
export const IMAGES = images;
export const ROUTES = routes;
export const SERVICES = services;
export const TEXT = text;

export const AUTHORIZATION = 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==';
export const EXPIRE_TIME_STORAGE = 'dashboard_performance_system_expire_time';
export const TOKEN_STORAGE = 'dashboard_performance_system_access_token';
export const COUNT_PAGE = 'count_page';
export const USER_DATA_STORAGE = 'dashboard_performance_system_user_data';
