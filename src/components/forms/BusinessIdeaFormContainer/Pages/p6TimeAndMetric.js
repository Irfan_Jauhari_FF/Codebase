import React from 'react';
import Grid from '@material-ui/core/Grid';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';
import FieldDate from '../../../elements/FieldDate';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { errors, touched, setFieldTouched, setFieldValue, handleAutosave, values } = this.props;

    return (
      <React.Fragment>
        <h3>Time of Implementation and Metric</h3>
        <p className="sub-text">
          This page identify the plan of team&#39;s target along its explanation.
        </p>
        <Field
          name="execDate"
          render={({ field }) => (
            <FieldDate
              {...field}
              data-cy="exec-date"
              dateFormat="DD/MM/YYYY"
              error={errors.execDate}
              isRange
              label="Time of Implementation"
              onChange={(e) => {
                setFieldValue('execDate', e);
                handleAutosave;
              }}
              placeholder="Start Date ~ End Date"
              required
              touched={touched.execDate}
              useFormattedValue
            />
          )}
        />

        <Grid container direction="column">
          <Grid className="field-group" container>
            <Grid item>
              <Field
                name="omtm"
                render={({ field }) => (
                  <FieldText
                    {...field}
                    data-cy="omtm-name"
                    error={errors.omtm}
                    label="Metric OMTM"
                    long
                    onChange={(e) => {
                      setFieldTouched('omtm', true);
                      setFieldValue('omtm', e.target.value);
                      handleAutosave;
                    }}
                    placeholder={placeholder.shortText('metric OMTM')}
                    required
                    touched={touched.omtm}
                    type="text"
                  />
                )}
              />
            </Grid>
            <Grid item>
              <Field
                name="omtmTarget"
                render={({ field }) => (
                  <FieldText
                    {...field}
                    data-cy="omtm-target"
                    decimal
                    error={errors.omtmTarget}
                    label="Target"
                    long
                    max={12}
                    onChange={(e) => {
                      setFieldTouched('omtmTarget', true);
                      setFieldValue('omtmTargetFormatted', e.target.value);
                      setFieldValue('omtmTarget', e.target.rawValue);
                      handleAutosave;
                    }}
                    placeholder={placeholder.shortText('the target')}
                    required
                    touched={touched.omtmTarget}
                    type="number"
                    value={values.omtmTargetFormatted}
                  />
                )}
              />
            </Grid>
          </Grid>

          <Field
            name="omtmDesc"
            render={({ field }) => (
              <FieldText
                {...field}
                block
                data-cy="omtm-desc"
                error={errors.omtmDesc}
                label="Explanation"
                onChange={(e) => {
                  setFieldTouched('omtmDesc', true);
                  setFieldValue('omtmDesc', e.target.value);
                  handleAutosave;
                }}
                placeholder={placeholder.longText('the explanation', 700)}
                required
                touched={touched.omtmDesc}
                type="long"
              />
            )}
          />
        </Grid>
      </React.Fragment>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
