import React from 'react';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';
import FieldSelect from '../../../elements/FieldSelect';
import { placeholder } from '../../../../constants/copywriting';
import { decisionList } from '../../../../utils/dropdownList';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { errors, touched, setFieldValue, setFieldTouched, handleAutosave, values } = this.props;

    const sectionBox = {
      padding: '1rem',
      borderRadius: '0.3125rem',
      boxShadow: '0 0 1px 0 rgb(72, 122, 157)',
      marginBottom: '1rem',
      whiteSpace: 'pre-wrap',
    };

    const blankFeedbackPlaceholder = 'No Feedback';

    return (
      <>
        <h3>Evaluation Summary</h3>
        <p className="sub-text">
          This page identify information about the summary of evaluations given by Tribe Zero.
        </p>

        <section style={sectionBox}>
          <h4 style={{ marginBottom: '0.5rem' }}>General and Background</h4>
          <h5>
            <strong>Evaluation of General and Background</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackGeneral !== '' ? values.feedbackGeneral : blankFeedbackPlaceholder}
          </p>
        </section>
        <section style={sectionBox}>
          <h4 style={{ marginBottom: '0.5rem' }}>Executive Summary</h4>
          <h5>
            <strong>Evaluation of Background</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackBackground !== ''
              ? values.feedbackBackground
              : blankFeedbackPlaceholder}
          </p>
          <h5>
            <strong>Evaluation of Description of Business Idea</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackDesc !== '' ? values.feedbackDesc : blankFeedbackPlaceholder}
          </p>
          <h5>
            <strong>Evaluation of Value Creation - Benefit for Telkom</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackTelkomBenefit !== ''
              ? values.feedbackTelkomBenefit
              : blankFeedbackPlaceholder}
          </p>
          <h5>
            <strong>Evaluation of Value Creation - Benefit for Customer</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackCustomerBenefit !== ''
              ? values.feedbackCustomerBenefit
              : blankFeedbackPlaceholder}
          </p>
          <h5>
            <strong>Evaluation of Value Capture</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackValueCapture !== ''
              ? values.feedbackValueCapture
              : blankFeedbackPlaceholder}
          </p>
        </section>
        <section style={sectionBox}>
          <h4 style={{ marginBottom: '0.5rem' }}>Canvassing of Product</h4>
          <h5>
            <strong>Evaluation of Golden Circle</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackGoldenCircle !== ''
              ? values.feedbackGoldenCircle
              : blankFeedbackPlaceholder}
          </p>
          <h5>
            <strong>Evaluation of Value Proposition Canvas</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackValuePropCustomer !== ''
              ? values.feedbackValuePropCustomer
              : blankFeedbackPlaceholder}
          </p>
          <h5>
            <strong>
              {values.additionalCanvasName === 'Lean Canvas'
                ? 'Evaluation of Lean Canvas'
                : 'Evaluation of Business Model Canvas'}
            </strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackAdditionalCanvas !== ''
              ? values.feedbackAdditionalCanvas
              : blankFeedbackPlaceholder}
          </p>
        </section>

        <section style={sectionBox}>
          <h4 style={{ marginBottom: '0.5rem' }}>Time of Implementation and Metric</h4>
          <h5>
            <strong>Evaluation of Time of Implementation</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackExecDate !== '' ? values.feedbackExecDate : blankFeedbackPlaceholder}
          </p>
          <h5>
            <strong>Evaluation of Metric OMTM</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackOmtm !== '' ? values.feedbackOmtm : blankFeedbackPlaceholder}
          </p>
        </section>

        <section style={sectionBox}>
          <h4 style={{ marginBottom: '0.5rem' }}>Resource Submission</h4>
          <h5>
            <strong>Evaluation of Proposed Talent</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackSquad !== '' ? values.feedbackSquad : blankFeedbackPlaceholder}
          </p>
          <h5>
            <strong>Evaluation of Proposed Budget</strong>
          </h5>
          <p className="caption" style={{ marginBottom: '0.5rem' }}>
            {values.feedbackBudget !== '' ? values.feedbackBudget : blankFeedbackPlaceholder}
          </p>
        </section>

        <Field
          name="feedbackSummary"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-summary"
              error={errors.feedbackSummary}
              info="This section identifies an evaluation's overview by Tribe Zero regarding the whole business idea proposals."
              label="Overview of Evaluations"
              onChange={(e) => {
                setFieldTouched('feedbackSummary', true);
                setFieldValue('feedbackSummary', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`overview of evaluations`, 700)}
              required
              touched={touched.feedbackSummary}
              type="long"
            />
          )}
        />

        <Field
          name="decision"
          render={({ field }) => (
            <FieldSelect
              {...field}
              dataCy="action"
              error={errors.decision}
              info="This evaluation form will be sent to DIC by accepting this action or will be sent to Business Unit by rejecting this action"
              label="Action"
              onBlur={() => setFieldTouched('decision', true)}
              onChange={(e) => {
                setFieldValue('decision', e);
              }}
              options={decisionList}
              placeholder={placeholder.select('action')}
              required
              touched={touched.decision}
            />
          )}
          type="select"
        />
      </>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
