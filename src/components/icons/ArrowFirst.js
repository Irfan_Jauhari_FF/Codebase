import React from 'react';
import PropTypes from 'prop-types';

export default function ArrowFirst(props) {
  const disabled = !props.disabled ? '#1E2025' : '#C8D0E0';

  return (
    <svg height="1.5rem" viewBox="0 0 24 24" width="1.5rem" xmlns="http://www.w3.org/2000/svg">
      <g fill="none" fillRule="evenodd">
        <path d="M10.683 11.924l5.62-5.217a.67.67 0 1 1 .911.982l-4.561 4.235a.74.74 0 0 0 0 1.085l4.605 4.277a.67.67 0 0 1-.911.982l-5.664-5.259a.74.74 0 0 1 0-1.085z" fill={disabled} fillRule="nonzero" />
        <path  d="M7.5 17.873V6.696" stroke={disabled} strokeLinecap="round" />
      </g>
    </svg>
  );
}

ArrowFirst.defaultProps = {
  disabled: false
};

ArrowFirst.propTypes = {
  disabled: PropTypes.bool,
};
