import React from 'react';
import PropTypes from 'prop-types';
import Pagination from '../../elements/Pagination';
import styles from './styles.css';
import { ICONS } from './../../../configs';
import { currency, numberWithPoint } from '../../../utils/format';

export default class Component extends React.Component {
  _renderHeader(row) {
    return (
      <table className={styles.table}>
        <thead>
          <tr className={styles['tr-two']}>
            <td className={styles['product-name']}>
              <img className={styles['icon-product']} src={row.productImage ? row.productImage : ICONS.ICON_NO_IMAGE} />
              <div>
                <h5 className={styles['th-first']}>Product Name</h5>
                <h4>{row.productName}</h4>
              </div>
            </td>
            <td className={styles['tribe-name']}>
              <div>
                <h5 className={styles['th-first']}>Tribe Name</h5>
                <h4>{row.tribeName}</h4>
              </div>
            </td>
          </tr>
        </thead>
      </table>
    );
  }

  _renderContent(row) {
    return (
      <table className={styles['table-content']}>
        <thead>
          <tr>
            <th className={styles['header-metric']}>Metric</th>
            <th className={styles['header-achievement']}>Current Achievement </th>
            <th className={styles['header-null']} />
            <th className={styles['header-achievement']}>MoM Growth </th>
            <th className={styles['header-null']} />
          </tr>
        </thead>
        <tbody className={styles.tbody}>
          {row.details.map((item, idx) => (
            <tr key={idx}>
              <td className={styles['header-metric']}>
                {item.metricUnit === 'Rupiah' ? <span>{item.metricName} <img className={styles.currency} src={ICONS.ICON_CURRENCY} /></span> : item.metricName}
              </td>
              <td className={styles['header-achievement']}>{item.metricUnit === 'Rupiah' ? currency(item.currentTotal) : `${numberWithPoint(item.currentTotal)} ${item.metricUnit}`}</td>
              <td className={styles['header-null']} />
              <td className={styles['header-achievement']}>{item.mom}%</td>
              <td className={styles['header-null']} />
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  _renderNoData() {
    return(
      <main className={styles['empty-container']}>
        <div>
          <header className={styles['empty-image']}>
            <img src={ICONS.ICON_NO_DATA} />
          </header>
          <h4 className={styles['empty-text']}>Sorry, no results were found.</h4>
        </div>
      </main>
    );
  }

  render() {
    const { data, isLoading, meta } = this.props;

    if (!isLoading && data) {
      if(data.length === 0) {
        return(
          <>{this._renderNoData()}</>
        );
      } else {
        return (
          <>
            {data.map((row, idx) => (
              <main className={styles.container} key={idx}>
                {this._renderHeader(row)}
                {this._renderContent(row)}
              </main>
            ))}
            {meta ? <Pagination className={styles.pagination} meta={meta} onChangeUrl={this.props.onChangeUrl} /> : ''}
          </>
        );
      }
    } else return null;
  }
}

Component.defaultProps = {
  data: [],
  isLoading: false,
  meta: {},
  onChangeUrl: {},
};

Component.propTypes = {
  data: PropTypes.array,
  isLoading: PropTypes.bool,
  meta: PropTypes.object,
  onChangeUrl: PropTypes.func,
};
