import React from 'react';
import PropTypes from 'prop-types';
import DialogContent from '@material-ui/core/DialogContent';
import styles from '../styles.css';

export default class Component extends React.Component {
  render() {
    return (
      <DialogContent {...this.props} className={styles['modal-content']}>
        {this.props.children}
      </DialogContent>
    );
  }
}

Component.defaultProps = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
};

Component.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
};
