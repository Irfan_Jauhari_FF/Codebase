import React from 'react';
import PropTypes from 'prop-types';
import Accordion from '../Accordion';

export default class Component extends React.Component {
  render() {
    const { data } = this.props;

    return (
      <>
        {data.map((item, idx) => (
          <Accordion desc={item.desc} key={idx} name={item.name} />
        ))}
      </>
    );
  }
}

Component.defaultProps = {
  data: [
    {
      name: <h4>One Two</h4>,
      desc: <h1>Anda sugab ilakes</h1>
    }
  ]
};

Component.propTypes = {
  data: PropTypes.array,
};
