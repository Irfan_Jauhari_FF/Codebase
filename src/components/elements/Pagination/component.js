import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import ArowFirst from '../../icons/ArrowFirst';
import ArrowLast from '../../icons/ArrowLast';
import ArrowNext from '../../icons/ArrowNext';
import ArrowPrevious from '../../icons/ArrowPrevious';
import styles from './styles.css';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this._handleCLickNext = this._handleCLickNext.bind(this);
    this._handleCLickPrevious = this._handleCLickPrevious.bind(this);
    this._handleCLickFirstPage = this._handleCLickFirstPage.bind(this);
    this._handleCLickLastPage = this._handleCLickLastPage.bind(this);
  }
  
  _handleCLickNext() {
    const query = queryString.parse(location.search);
    const { onChangeUrl } = this.props;

    if (query.page) {
      onChangeUrl('page', parseInt(query.page) + 1);
    } else {
      onChangeUrl('page', 2);
    }
  }

  _handleCLickPrevious() {
    const query = queryString.parse(location.search);
    const { onChangeUrl } = this.props;
    onChangeUrl('page', parseInt(query.page) - 1);
  }

  _handleCLickFirstPage() {
    this.props.onChangeUrl('page', 1);
  }

  _handleCLickLastPage() {
    const { onChangeUrl, meta } = this.props;
    onChangeUrl('page', parseInt(meta.totalPage));
  }

  _renderFirstPage() {
    const { meta } = this.props;
    
    if(meta.page === 1) {
      return(
        <span className={styles.arrow} style={{ cursor: 'default' }} >
          <ArowFirst disabled={meta.page === 1} />
        </span>
      );
    } else {
      return(
        <span className={styles.arrow} onClick={this._handleCLickFirstPage} >
          <ArowFirst disabled={meta.page === 1} />
        </span>
      );
    }
  }

  _renderPreviousPage() {
    const { meta } = this.props;
    
    if(meta.page === 1) {
      return(
        <span className={styles.arrow} style={{ cursor: 'default' }} >
          <ArrowPrevious disabled={meta.page === 1} />
        </span>
      );
    } else {
      return(
        <span className={styles.arrow} onClick={this._handleCLickPrevious} >
          <ArrowPrevious disabled={meta.page === 1} />
        </span>
      );
    }
  }

  _renderNextPage() {
    const { meta } = this.props;
    
    if(meta.page === meta.totalPage) {
      return(
        <span className={styles.arrow} style={{ cursor: 'default' }} >
          <ArrowNext disabled={meta.page === meta.totalPage} />
        </span>
      );
    } else {
      return(
        <span className={styles.arrow} onClick={this._handleCLickNext} >
          <ArrowNext disabled={meta.page === meta.totalPage} />
        </span>
      );
    }
  }

  _renderLastPage() {
    const { meta } = this.props;    
    
    if(meta.page === meta.totalPage) {
      return(
        <span className={styles.arrow} style={{ cursor: 'default' }} >
          <ArrowLast disabled={meta.page === meta.totalPage}/>
        </span>
      );
    } else {
      return(
        <span className={styles.arrow}  onClick={this._handleCLickLastPage} >
          <ArrowLast disabled={meta.page === meta.totalPage}/>
        </span>
      );
    }
  }

  render() {
    const { className, meta } = this.props;
    
    return (
      <main className={`${styles.main} ${className}`}>
        <h4 className={styles.page}>Page {meta.page} of {meta.totalPage}</h4>
        {this._renderFirstPage()}
        {this._renderPreviousPage()}
        {this._renderNextPage()}
        {this._renderLastPage()}
      </main>
    );
  }
}

Component.defaultProps = {
  className: '',
  meta: {
    page: 1,
    count: 10,
    totalPage: 1,
    totalData: 0
  },
  onChangeUrl: {},
};

Component.propTypes = {
  className: PropTypes.string,
  meta: PropTypes.object,
  onChangeUrl: PropTypes.func,
};
