import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './action';
import Component from './component';

const mapStateToProps = (state) => {
  return {
    sideDrawer: state.sideDrawer,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
};

const SideDrawerWithRedux = connect(mapStateToProps, mapDispatchToProps)(Component);

export default SideDrawerWithRedux;
