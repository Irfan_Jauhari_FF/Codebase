import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import Button from '../Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ICONS } from '../../../configs';

const CdxDrawer = withStyles({
  paper: {
    padding: '1.5rem',
    '& .close-button': {
      backgroundImage: `url(${ICONS.CLOSE_MINI_WHITE})`,
      backgroundSize: '0.8125rem 0.8125rem',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      position: 'fixed',
      transform: 'translate(-2.5rem, 0)',
      margin: 0,
      zIndex: 1400,
    },
  },
})(Drawer);

export default class Component extends React.Component {
  constructor() {
    super();
    this._handleCloseClick = this._handleCloseClick.bind(this);
  }

  _handleCloseClick() {
    this.props.actions.closeSideDrawer();
  }

  render() {
    const { children, className, sideDrawer } = this.props;

    return (
      <div className={`${className}`}>
        <CdxDrawer anchor="right" open={sideDrawer.isOpen}>
          <Button circle className="close-button" onClick={this._handleCloseClick} />
          {children}
        </CdxDrawer>
      </div>
    );
  }
}

Component.defaultProps = {
  children: null,
  className: '',
};

Component.propTypes = {
  actions: PropTypes.object.isRequired,
  children: PropTypes.node,
  className: PropTypes.string,
  sideDrawer: PropTypes.object.isRequired,
};
