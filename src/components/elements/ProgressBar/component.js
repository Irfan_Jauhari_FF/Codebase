import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

export default class Component extends React.Component {
  render() {
    const { percentage } = this.props;
    const width = (280 * percentage / 100) / 16;

    return (
      <section className={styles['container']}>
        <div className={styles['content']}>
          <div className={styles['success']} style={{ width: `${width}rem` }} />
        </div>
        <h5>{percentage} %</h5>
      </section>
    );
  }
}

Component.defaultProps = {
  percentage: 100
};

Component.propTypes = {
  percentage: PropTypes.number
};
